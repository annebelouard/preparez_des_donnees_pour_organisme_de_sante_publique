# Préparez_des_donnees_pour_organisme_de_santé_publique
OCR | Data Scientist | Projet 3 | 70 heures


## Contexte

L'agence Santé publique France souhaite améliorer sa base de données Open Food Facts et fait appel aux services de notre entreprise. Cette base de données open source est mise à la disposition de particuliers et d’organisations afin de leur permettre de connaître la qualité nutritionnelle de produits. 

Aujourd’hui, pour ajouter un produit à la base de données d'Open Food Facts, il est nécessaire de remplir de nombreux champs textuels et numériques, ce qui peut conduire à des erreurs de saisie et à des valeurs manquantes dans la base. 

L’agence Santé publique France confie à notre entreprise la création d’un système de suggestion ou d’auto-complétion pour aider les usagers à remplir plus efficacement la base de données. 



 
